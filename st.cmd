require essioc
require ethercatmc

epicsEnvSet("MOTOR_PORT",    "$(SM_MOTOR_PORT=MCU1)")

epicsEnvSet("IPADDR",        "$(SM_IPADDR=10.102.10.24)")
epicsEnvSet("IPPORT",        "$(SM_IPPORT=48898)")
epicsEnvSet("ASYN_PORT",     "$(SM_ASYN_PORT=MC_CPU1)")
epicsEnvSet("PREFIX",        "YMIR-SpScn:MC-MCU-001:")
epicsEnvSet("PREC",          "$(SM_PREC=3)")
epicsEnvSet("SM_NOAXES",     "3")


epicsEnvSet("ECM_OPTIONS",   "adsPort=852;amsNetIdRemote=172.30.244.59.1.1;amsNetIdLocal=10.102.10.13.1.1")

# e3 common databases
iocshLoad("$(essioc_DIR)/common_config.iocsh")


# No poller yet, see ethercatmcStartPoller at the end of the script
epicsEnvSet("ECM_MOVINGPOLLPERIOD", "0")
epicsEnvSet("ECM_IDLEPOLLPERIOD", "0")
iocshLoad("$(ethercatmc_DIR)/ethercatmcController.iocsh")

#
#
#
iocshLoad("$(ethercatmc_DIR)/ethercatmcEL6688-PTP.iocsh")
iocshLoad("$(ethercatmc_DIR)/ethercatmcEL6688-ExtSync.iocsh")


#
# Analog input
#
epicsEnvSet("P",             "YMIR-SpScn:MC-MCU-001:")
epicsEnvSet("R",             "AnalogInput")
epicsEnvSet("DESC",          "LaserDiodeSensor")
epicsEnvSet("ASYNPARAMNAME", "AnalogInput")
epicsEnvSet("CHNO",          "0")
epicsEnvSet("ECEL3162FIELDINIT", "")
iocshLoad("$(ethercatmc_DIR)/ethercatmcEL3162-OneChannel.iocsh")

#
# Analog input.
# Same as before, but timestamped with the
# UTC from the PTP timing server.
#
epicsEnvSet("R",             "AnalogInput-UTC")
epicsEnvSet("DESC",          "LaserDiodeSensor-TSE")
epicsEnvSet("ASYNPARAMNAME", "AnalogInput")
epicsEnvSet("CHNO",          "0")
epicsEnvSet("ECEL3162FIELDINIT", ", TSE=-2")
iocshLoad("$(ethercatmc_DIR)/ethercatmcEL3162-OneChannel.iocsh")
epicsEnvSet("ECEL3162FIELDINIT", "")


#
# Axis 1
#
epicsEnvSet("AXISCONFIG",    "")
epicsEnvSet("PREFIX",        "YMIR-SpScn:MC-X-01:")
epicsEnvSet("MOTOR_NAME",    "$(SM_MOTOR_NAME=Mtr)")
epicsEnvSet("AXIS_NO",       "$(SM_AXIS_NO=1)")
epicsEnvSet("DESC",          "$(SM_DESC=DESC)")
epicsEnvSet("EGU",           "$(SM_EGU=EGU)")
iocshLoad("$(ethercatmc_DIR)/ethercatmcIndexerAxis.iocsh")
iocshLoad("$(ethercatmc_DIR)/ethercatmcAxisdebug.iocsh")

# Predefined Positions
# NVL: Input Value Location (INLINK)
epicsEnvSet("NVL",           "")
epicsEnvSet("EGU",           "")
iocshLoad("$(ethercatmc_DIR)/ethercatmcDefPos.iocsh")



#
# Axis 2
#
epicsEnvSet("AXISCONFIG",    "")
epicsEnvSet("PREFIX",        "YMIR-SpScn:MC-Y-01:")
epicsEnvSet("MOTOR_NAME",    "$(SM_MOTOR_NAME=Mtr)")
epicsEnvSet("AXIS_NO",       "$(SM_AXIS_NO=2)")
iocshLoad("$(ethercatmc_DIR)/ethercatmcIndexerAxis.iocsh")
iocshLoad("$(ethercatmc_DIR)/ethercatmcAxisdebug.iocsh")
# Predefined Positions
# NVL: Input Value Location (INLINK)
epicsEnvSet("NVL",           "")
epicsEnvSet("EGU",           "")
iocshLoad("$(ethercatmc_DIR)/ethercatmcDefPos.iocsh")


#
# Axis 3
#
epicsEnvSet("AXISCONFIG",    "")
epicsEnvSet("PREFIX",        "YMIR-SpScn:MC-Z-01:")
epicsEnvSet("MOTOR_NAME",    "$(SM_MOTOR_NAME=Mtr)")
epicsEnvSet("AXIS_NO",       "$(SM_AXIS_NO=3)")
iocshLoad("$(ethercatmc_DIR)/ethercatmcIndexerAxis.iocsh")
iocshLoad("$(ethercatmc_DIR)/ethercatmcAxisdebug.iocsh")
# Predefined Positions
# NVL: Input Value Location (INLINK)
epicsEnvSet("NVL",           "")
epicsEnvSet("EGU",           "")
iocshLoad("$(ethercatmc_DIR)/ethercatmcDefPos.iocsh")


#
# Start polling, values are in millisconds
#
epicsEnvSet("MOVINGPOLLPERIOD", "10")
epicsEnvSet("IDLEPOLLPERIOD",   "100")
ethercatmcStartPoller("$(MOTOR_PORT)", "$(MOVINGPOLLPERIOD)", "$(IDLEPOLLPERIOD)")

iocinit()
